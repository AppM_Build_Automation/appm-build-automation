AppM Build Automaiton is an open source and automated software application that is used to construct and build projects on different operating systems and using different development environments.

Benefits:

* Cross-platform;

* Open source;

* Easy scripting language;

* There is no reference to any particular programming language;

* Can be used inside other build systems;

* Building project algorithm.

General principles of implementation:

Each project must have a Build Project File, containing the necessary information on the construction of the project. For example, a unique name of the project, dependence on other projects and libraries, methods of construction, etc. Thus, the entire repository should be marked by the project description files to provide unambiguous information on the projects and their dependencies.

AppM Build Automaiton must be able to perform different actions (project construction, external applications launch, file copying, etc.) which are needed to build a -product.

AppM Build Automaiton should provide an opportunity to create flexible scripting actions on construction projects. In this case , all operations with projects should be carried out on the basis of information that build tool derives from the project description files.

At the moment Build Project File uses am XML-file in the format described below. XML file is also used as a Job File.

*This software is released under the MIT License (MIT).*